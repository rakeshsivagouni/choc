package com.hubster.request;

public class Request {

	private int chidd;
	private String followupp;
	private String chassignid;
	public String getStaffe_id() {
		return staffe_id;
	}

	public void setStaffe_id(String staffe_id) {
		this.staffe_id = staffe_id;
	}

	private String staffe_id;
	private String duedatee;
	private String corpgoalstatus;
	private int cgoal_id;
	private int loginid;
	private int goal;
	private String assignoto;
	private int id;
	private String ccreatedby;
	private String cgoalgroup;
	private String loginuser;
	private String password;
	private String initiativestatusid;
	private int initiativeid;

	public int getLoginid() {
		return loginid;
	}

	public void setLoginid(int loginid) {
		this.loginid = loginid;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getLoginuser() {
		return loginuser;
	}

	public void setLoginuser(String loginuser) {
		this.loginuser = loginuser;
	}

	public String getCgoalgroup() {
		return cgoalgroup;
	}

	public void setCgoalgroup(String cgoalgroup) {
		this.cgoalgroup = cgoalgroup;
	}

	public int getInitiativeid() {
		return initiativeid;
	}

	public void setInitiativeid(int initiativeid) {
		this.initiativeid = initiativeid;
	}

	public String getInitiativestatusid() {
		return initiativestatusid;
	}

	public void setInitiativestatusid(String initiativestatusid) {
		this.initiativestatusid = initiativestatusid;
	}

	public String getAssignoto() {
		return assignoto;
	}

	public void setAssignoto(String assignoto) {
		this.assignoto = assignoto;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCcreatedby() {
		return ccreatedby;
	}

	public void setCcreatedby(String ccreatedby) {
		this.ccreatedby = ccreatedby;
	}

	public int getGoal() {
		return goal;
	}

	public void setGoal(int goal) {
		this.goal = goal;
	}

	// public String getUser_goal_id() {
	// return user_goal_id;
	// }
	//
	// public void setUser_goal_id(String user_goal_id) {
	// this.user_goal_id = user_goal_id;
	// }

	public String getCorpgoalstatus() {
		return corpgoalstatus;
	}

	public void setCorpgoalstatus(String corpgoalstatus) {
		this.corpgoalstatus = corpgoalstatus;
	}

	public int getCgoal_id() {
		return cgoal_id;
	}

	public void setCgoal_id(int cgoal_id) {
		this.cgoal_id = cgoal_id;
	}

	public String getDuedatee() {
		return duedatee;
	}

	public void setDuedatee(String duedatee) {
		this.duedatee = duedatee;
	}

	@Override
	public String toString() {
		return "Request [chidd=" + chidd + ", followupp=" + followupp + ", chassignid=" + chassignid + "]";
	}

	public String getChassignid() {
		return chassignid;
	}

	public void setChassignid(String chassignid) {
		this.chassignid = chassignid;
	}

	public int getChidd() {
		return chidd;
	}

	public void setChidd(int chidd) {
		this.chidd = chidd;
	}

	public String getFollowupp() {
		return followupp;
	}

	public void setFollowupp(String followupp) {
		this.followupp = followupp;
	}

}
