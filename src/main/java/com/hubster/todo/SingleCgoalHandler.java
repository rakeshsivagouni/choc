package com.hubster.todo;

import java.util.List;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.RequestStreamHandler;
import com.hubster.dao.StrategyDaoImpl;
import com.hubster.dao.TodoDaoImpl;
import com.hubster.model.CgoalModel;
import com.hubster.request.Request;
import com.hubster.response.CgoalsResponse;
import com.hubster.response.DialogAction;
import com.hubster.response.LexResponse;
import com.hubster.response.Message;

public class SingleCgoalHandler implements RequestHandler<Request, LexResponse> {
	
	private final StrategyDaoImpl singleCgoalService = StrategyDaoImpl.instance;

	private CgoalModel getcgoal() {
		return new CgoalModel();
	}

	@Override
	public LexResponse handleRequest(Request input, Context context) {
		List<CgoalModel> cgoallist = singleCgoalService.getsinglecgoal(input);

		CgoalModel res = getcgoal();
		if (!cgoallist.isEmpty()) {

			// res.setResCode(CommonContants.SUCCESS_CODE);
			// res.setResDesc(CommonContants.SUCCSS_DESC);
			res.setGoalmodel(cgoallist);

		} else {
			// res.setResCode(CommonContants.FAIL_CODE);
			// res.setResDesc(CommonContants.FAIL_DESC);
		}
		
		Message message = new Message(cgoallist.toString());
//		DialogAction dialogueAction = new DialogAction(message);

		return new LexResponse();
//		return res;
	}



}
