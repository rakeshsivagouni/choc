package com.hubster.todo;

import java.util.List;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.hubster.dao.StrategyDaoImpl;
import com.hubster.model.ActiveStrategyModel;
import com.hubster.request.Request;
import com.hubster.response.Message;

public class ActiveStrategiesHandler implements RequestHandler<Request, String> {

	private final StrategyDaoImpl activestrategiesService = StrategyDaoImpl.instance;

	private ActiveStrategyModel getResponse() {
		return new ActiveStrategyModel();
	}

	@Override
	public String handleRequest(Request input, Context context) {

		List<ActiveStrategyModel> activelist = activestrategiesService.activestrategygoals(input);

		ActiveStrategyModel res = getResponse();
		if (!activelist.isEmpty()) {
			res.setGetactiveStratagies(activelist);

		} else {
		}
		Message message = new Message(activelist.toString());
		// DialogAction dialogueAction = new DialogAction(message);

		// return new LexResponse(message);
		// return res;

		return activelist.toString();
	}

}
