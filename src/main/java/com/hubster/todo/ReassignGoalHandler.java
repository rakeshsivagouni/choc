package com.hubster.todo;

import java.util.List;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.hubster.dao.TodoDaoImpl;
import com.hubster.model.HubbiGoals;
import com.hubster.request.Request;
import com.hubster.response.DialogAction;
import com.hubster.response.LexResponse;
import com.hubster.response.Message;

public class ReassignGoalHandler implements RequestHandler<Request, LexResponse> {

	
	private final TodoDaoImpl reassginService = TodoDaoImpl.instance;

	private HubbiGoals getResponse() {
		return new HubbiGoals();
	}

	@Override
	// public LexResponse handleRequest(Request input, Context context) {

	public LexResponse handleRequest(Request input, Context context) {

		List<HubbiGoals> reassignlist = reassginService.reassign_goal(input);

		HubbiGoals res = getResponse();
		if (!reassignlist.isEmpty()) {
			res.setHgmodel(reassignlist);
			

		} else {
		}
		 Message message = new Message(reassignlist.toString());
//		 DialogAction dialogueAction = new DialogAction(message);
		
		 return new LexResponse();
//		return res;
	}


}
